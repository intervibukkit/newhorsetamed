package InterVi.NewHorseTamed;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.Iterator;

import InterVi.NewHorseTamed.config.pack;

public class main extends JavaPlugin implements Listener {
class logger {
	private final Logger log = Logger.getLogger("Minecraft");
	void info(String text) {
		log.info("[NewHorseTamed] " + text);
	}
}
logger log = new logger(); //для отправки сообщений в консоль
config conf = new config(); //грузим конфиг
events ev = null;

private String remChars(String s, int p1, int p2) { //метод для вырезания символов из строк
	   String pp1, pp2, result;
	   if (p1 > -1 && p2 > p1 && p2 <= s.length()) {
		   	int pr = p2 - p1; pr = p1 + pr;
	   		if (p1 != 0) pp1 = s.substring(0, p1); else pp1 = s;
	   		if(p2 == s.length()) {if (p2 != pr) pp2 = s.substring(pr, p2); else pp2 = s;} else pp2 = s.substring(p2, s.length());
	   		if (pp1.equals(s)) result = pp2; else if (pp2.equals(s)) result = pp1; else result = pp1 + pp2;
	   	} else result = s;
	   return result;
}

public void onEnable() {
	log.info("<--------------------->");
	log.info("Старт NewHorseTamed...");
	getServer().getPluginManager().registerEvents(new events(this), this);
	log.info("{ Created by InterVi }");
	log.info("{ Идея Acurity }");
	log.info("<--------------------->");
}

public void onDisable() {
	log.info("Деактивация плагина, выдача седел...");
	if (ev != null) {
		Player pls[] = null;
		int psize = Bukkit.getServer().getOnlinePlayers().size();
		pls = new Player[psize];
		for (int i = 0; i < psize; i++) {
			pls[i] = (Player) Bukkit.getServer().getOnlinePlayers().iterator().next();
		}
		if (pls != null) {
		for (int i = 0; i < pls.length; i++) {
			if (pls[i].isInsideVehicle()) {
				Entity entity = pls[i].getVehicle();
				if (ev.SaddleGive(pls[i], entity)) {
					Location loc = pls[i].getLocation();
					int y = loc.getBlockY();
					loc.setY(y+1); //чтобы игрок не провалился
					pls[i].teleport(loc);
				}
			}
		}}
	}
}

private boolean GiveApple(String nick, int amount) { //выдача яблока
	int am[] = {amount};
	if (amount > 64) { //разбиваем на стаки
		int lng = (int) Math.ceil((amount / 64));
		am = new int[lng];
		for (int i = 0; i < lng; i++) {
			amount -= 64;
			if (amount >= 64 | amount == 0) am[i] = 64; else am[i] = amount;
		}
	}
	Player pls[] = null;
	int psize = Bukkit.getServer().getOnlinePlayers().size();
	pls = new Player[psize];
	Iterator<? extends Player> iter =  Bukkit.getServer().getOnlinePlayers().iterator();
	for (int i = 0; i < psize; i++) {
		pls[i] = iter.next();
	}
	boolean gived = false, dropped = false;
	int play = -1;
	ItemStack inv[] = null;
	if (pls != null) {
	for (int i = 0; i < pls.length; i++) {
		if (nick.equals(pls[i].getPlayerListName())) {
			play = i;
			break;
		}
	}}
	if (play > -1) {
		inv = pls[play].getInventory().getContents();
	for (int a = 0; a < am.length; a++) {
		ItemStack apple = new ItemStack(conf.appleid); //создаем яблоко
		ItemMeta meta = apple.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', conf.apple));
		apple.setItemMeta(meta);
		apple.setAmount(am[a]); //и задаем ему количество
			int slot = -1;
			for (int n = 0; n < inv.length; n++) { //ложим яблоко в стак
				if (inv[n] != null) {
				if (inv[n].getTypeId() == conf.appleid & (inv[n].getAmount()+am[a]) <= 64 && inv[n].getItemMeta() != null) {
					if (inv[n].getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', conf.apple))) {
						slot = n;
						int newamount = inv[n].getAmount()+am[a];
						apple.setAmount(newamount);
						break;
					}
				}
			}
			}
			if (slot == -1) {
			for (int i = 0; i < inv.length; i++) { //если не нашлось - выдаем
				if (inv[i] == null) {
					slot = i;
					break;
				}
			}
			}
			if (slot > -1) { //если в инвентаре есть место - выдаем
				inv[slot] = apple;
				gived = true;
			} else { //если нет - дропаем
				Location loc = pls[play].getLocation();
				loc.getWorld().dropItemNaturally(loc, apple);
				dropped = true;
			}
		}}
	if (play > -1) { //устанавливаем инвентарь и отправляем уведомление
		if (gived) {
			if (inv != null) {
				pls[play].getInventory().setContents(inv);
				if (!dropped) pls[play].sendMessage("Яблоко выдано");
			}
		}
		if (dropped) {
			pls[play].sendMessage("Инвентарь заполнен, яблоко дропнуто");
		}
	}
	if (play > -1) return true; else return false;
}

public boolean GiveSaddle(String nick, pack p) {
	Player pls[] = null;
	int psize = Bukkit.getServer().getOnlinePlayers().size();
	pls = new Player[psize];
	Iterator<? extends Player> iter =  Bukkit.getServer().getOnlinePlayers().iterator();
	for (int i = 0; i < psize; i++) {
		pls[i] = iter.next();
	}
	int pl = -1;
	if (pls != null) {
	for (int i = 0; i < pls.length; i++) {
		if (nick.equals(pls[i].getPlayerListName())) {
			pl = i;
			break;
		}
	}}
	if (pl > -1) {
	ItemStack item = new ItemStack(329); //создаем седло
	ItemMeta meta = item.getItemMeta();
	meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', p.displayname));
	ArrayList<String> list = new ArrayList<String>();
	list.add(ChatColor.translateAlternateColorCodes('&', p.lore));
	if (!conf.hide) {
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Тип: " + "&c" + p.type));
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Цвет: " + "&c" + p.color));
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Стиль: " + "&c" + p.style));
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Ребенок: " + "&c" + String.valueOf(p.baby).replaceAll("true", "да").replaceAll("false", "нет")));
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Высота прыжка: " + "&c" + String.valueOf(p.jump)));
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Есть сундук: " + "&c" + String.valueOf(p.chest).replaceAll("true", "да").replaceAll("false", "нет")));
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Есть седло: " + "&c" + String.valueOf(p.saddle).replaceAll("true", "да").replaceAll("false", "нет")));
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Броня: " + "&c" + p.armor));
	list.add(ChatColor.translateAlternateColorCodes('&', "&6Можно бить: " + "&c" + String.valueOf(p.damage).replaceAll("true", "да").replaceAll("false", "нет")));
	}
	int s = list.size();
	meta.setLore(list.subList(0, s));
	item.setItemMeta(meta);
	int slot = -1;
	ItemStack inv[] = pls[pl].getInventory().getContents();
	for (int i2 = 0; i2 < inv.length; i2++) {
		if (inv[i2] == null) {
			slot = i2;
			break;
		}
	}
	if (slot > -1) { //если в инвентаре есть место - выдаем
		inv[slot] = item;
		pls[pl].getInventory().setContents(inv);
		pls[pl].sendMessage("Седло выдано");
	} else {
		Location loc = pls[pl].getLocation();
		loc.getWorld().dropItemNaturally(loc, item); // если нет - дропаем
		pls[pl].sendMessage(ChatColor.translateAlternateColorCodes('&', conf.noslot));
	}
	}
	if (pl > -1) return true; else return false;
}

public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
	if (command.getName().equalsIgnoreCase("nht-reload") && sender.hasPermission("newhorsetamed.reload") | sender.isOp()) {
		conf.load();
		sender.sendMessage("[NewHorseTamed] конфиг перезагружен");
		return true;
	} else if (command.getName().equalsIgnoreCase("nht-packs") && sender.hasPermission("newhorsetamed.get") | sender.isOp()) {
		pack p[] = conf.packs;
		for (int i = 0; i < p.length; i++) sender.sendMessage(ChatColor.translateAlternateColorCodes('&', p[i].displayname));
		return true;
	} else if (command.getName().equalsIgnoreCase("nht-get") && sender.hasPermission("newhorsetamed.get") | sender.isOp()) {
		if (sender instanceof Player) {
		Player player = (Player) sender;
		if (args != null && args.length > 0) {
			String pn = args[0];
			for (int i = 1; i < args.length; i++) pn += " " + args[i];
			pn = pn.trim();
			if (pn != null && pn.length() > 0) {
				pack p[] = conf.packs;
				boolean gived = false;
				for (int i = 0; i < p.length; i++) {
					String dn = p[i].displayname;
					while (dn.indexOf("&") > -1) {
						if ((dn.lastIndexOf(" ") + 2) >= dn.length()) break;
						dn = remChars(dn, dn.indexOf("&"), dn.indexOf("&")+2);
					}
					dn = dn.trim();
					if (pn.equalsIgnoreCase(dn)) {
						gived = GiveSaddle(player.getPlayerListName(), p[i]);
						break;
					}
				}
				if (!gived) player.sendMessage("Пак не найден, nht-get пак");
			}
		} else player.sendMessage("Введите название пака, nht-get пак");
		} else sender.sendMessage("[NewHorseTamed] Эту команду можно выполнять только из игры");
		return true;
	} else if (command.getName().equalsIgnoreCase("nht-apple") && sender.hasPermission("newhorsetamed.apple") | sender.isOp()) {
		ItemStack apple = new ItemStack(conf.appleid);
		ItemMeta meta = apple.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', conf.apple));
		apple.setItemMeta(meta);
		apple.setAmount(1);
		boolean is = true;
		if (args != null) {
			if (args.length > 0) {
				if (args[0] != null) is = false;
			}
		}
		if (sender instanceof Player && is) {
			String nick = ((Player) sender).getPlayerListName();
			GiveApple(nick, 1);
		} else if (!is) {
			String nick = args[0];
			int amount = 1;
			if (args.length > 1) {
				if (args[1] != null) {
					try {
						amount = Integer.parseInt(args[1]);
					} catch(Exception e) {sender.sendMessage("[NewHorseTamed] Неправильное число, nht-apple ник число");}
				}
			}
			if (!GiveApple(nick, amount)) sender.sendMessage("[NewHorseTamed] Игрок не найден, nht-apple ник"); else sender.sendMessage("[NewHorseTamed] Яблоко выдано");
		} else if (is) sender.sendMessage("[NewHorseTamed] Выдать яблоко игроку: nht-apple ник"); 
		return true;
	} else if (command.getName().equalsIgnoreCase("nht-help") && sender.hasPermission("newhorsetamed.help") | sender.isOp()) {
		sender.sendMessage("<--------------------->");
		sender.sendMessage("/nht-reload - перезагрузить конфиг");
		sender.sendMessage("/nht-apple [игрок] [количество] - получить яблоко");
		sender.sendMessage("/nht-packs - посмотреть названия паков");
		sender.sendMessage("/nht-get [пак] - получить пак (седло)");
		sender.sendMessage("/nht-help - справка");
		sender.sendMessage("<--------------------->");
		sender.sendMessage("Created by InterVi");
		sender.sendMessage("Идея Acurity");
		sender.sendMessage("<--------------------->");
		return true;
	}
	else return false;
}
}