package InterVi.NewHorseTamed;

public class config {
	config() {load();}
	
	boolean enabled = true;
	int percent = 10;
	String otherrename = "&cАй ай ай!";
	int appleid = 322;
		String apple = "&2Яблоко приручения";
	String notamed = "&eУдача не на твоей стороне, попробуй еще раз";
	String tamedtoplay = "&eУра! Ты получил седло!";
	int wait = 30;
		String waitmes = "&cПодожди &e%sec% &cсекнуд";
	boolean tamedmess = true;
		String tamednotif = "&e%nick% &2получил седло призыва &a%name%!";
	String saddlename = "&eСедло супер-лошади";
	String armorname = "&eБроня супер-лошади";
	String noslot = "&cИнвентарь забит, седло дропнуто";
	boolean hide = false;
	
	int buyid = 264;
	int buycol = 3;
	String nocol = "&cНедостаточно алмазов, нужно &e%col%";
	String noitem = "&cНе тот предмет, возьми %id%";
	String buyok = "&eСкорей держи яблоко!";
	String mobname = "Продавец яблок";
	boolean nodamage = true;
	String noperm = "&cНет прав";
		
	class pack {
		String displayname = "&eСтранная лошадь";
		String lore = "&eЭтим седлом можно призвать лошадь";
		String type = "HORSE";
		String color = "WHITE";
		String style = "NONE";
		boolean baby = false;
		double jump = 0.7;
		boolean chest = false;
		boolean saddle = true;
		String armor = "GOLD";
		boolean damage = true;
		pack getPack() {pack p = new pack(); return p;}
	}
	pack pack = new pack();
	
	pack packs[] = {pack};
	
	void load() { //загрузка данных из конфига
		configLoader loader = new configLoader();
		String file = loader.getPatch();
		if (System.getProperty("os.name" ).equalsIgnoreCase("Windows")) {
			file += "\\plugins\\NewHorseTamed\\config.yml";
		} else {
			file += "/plugins/NewHorseTamed/config.yml";
		}
		loader.saveConfig("/config.yml", file); //сохранение дефолтного конфига, если его нет
		loader.load(file);
		this.enabled = loader.getBoolean("enabled");
		this.tamedmess = loader.getBoolean("tamedmess");
		this.hide = loader.getBoolean("hide");
		this.nodamage = loader.getBoolean("nodamage");
		this.percent = loader.getInt("percent");
		this.appleid = loader.getInt("appleid");
		this.wait = loader.getInt("wait");
		this.buyid = loader.getInt("buyid");
		this.buycol = loader.getInt("buycol");
		this.otherrename = loader.getString("otherrename");
		this.apple = loader.getString("apple");
		this.notamed = loader.getString("notamed");
		this.tamedtoplay = loader.getString("tamedtoplay");
		this.tamednotif = loader.getString("tamednotif");
		this.saddlename = loader.getString("saddlename");
		this.armorname = loader.getString("armorname");
		this.waitmes = loader.getString("waitmes");
		this.noslot = loader.getString("noslot");
		this.nocol = loader.getString("nocol");
		this.noitem = loader.getString("noitem");
		this.buyok = loader.getString("buyok");
		this.mobname = loader.getString("mobname");
		this.noperm = loader.getString("noperm");
		
		String[] n = loader.getSectionNames("packs");
		packs = new pack[n.length];
		for (int i = 0; i < n.length; i++) {
			pack p = new pack();
			p.displayname = loader.getStringSection(n[i], "displayname");
			p.lore = loader.getStringSection(n[i], "lore");
			p.type = loader.getStringSection(n[i], "type");
			p.color = loader.getStringSection(n[i], "color");
			p.style = loader.getStringSection(n[i], "style");
			p.armor = loader.getStringSection(n[i], "armor");
			p.baby = loader.getBooleanSection(n[i], "baby");
			p.chest = loader.getBooleanSection(n[i], "chest");
			p.saddle = loader.getBooleanSection(n[i], "saddle");
			p.damage = loader.getBooleanSection(n[i], "damage");
			p.jump = loader.getDoubleSection(n[i], "jump");
			packs[i] = p;
		}
	}
}