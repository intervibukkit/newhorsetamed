package InterVi.NewHorseTamed;

import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.entity.Player;
import org.bukkit.entity.Entity;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Horse.Style;
import org.bukkit.entity.Horse.Color;
import org.bukkit.entity.Horse;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.ArrayList;

import InterVi.NewHorseTamed.config.pack;

public class events implements Listener {
	main main;
	events(main m) {
		main = m;
		main.ev = this;
		main.log.info("обработка событий активирована");
	}
	ArrayList<String> horses = new ArrayList<String>(); //для запоминания созданных лошадей
	ArrayList<String> times = new ArrayList<String>(); //для запоминания задержек
	
	@EventHandler(priority = EventPriority.LOW)
	public void onInteractEntity(PlayerInteractEntityEvent event) { //обработка клика по жителю
		if (main.conf.enabled) {
			Entity entity = event.getRightClicked();
			String name = entity.getCustomName();
			if (entity.getType().toString().equalsIgnoreCase("VILLAGER") && name != null) {
				if (name.equals(main.conf.mobname)) { //если кликнули по нужному жителю
					event.setCancelled(true); //отменяем ивент, чтобы не мешалась открытая менюха
					Player player = event.getPlayer();
					if (player.hasPermission("newhorsetamed.buy") | player.isOp()) { //если есть разрешение
						if (player.getInventory().getItemInHand().getTypeId() == main.conf.buyid) {
							int amount = player.getInventory().getItemInHand().getAmount();
							if (amount >= main.conf.buycol) { //если предмета достаточно - выдаем яблоко 
								int newamount = amount - main.conf.buycol;
								if (newamount > 0) player.getInventory().getItemInHand().setAmount(newamount); else {
									ItemStack item = new ItemStack(0);
									player.getInventory().setItemInHand(item);
								}
								Location loc = entity.getLocation();
								ItemStack item = new ItemStack(main.conf.appleid);
								ItemMeta meta = item.getItemMeta();
								meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.conf.apple));
								item.setItemMeta(meta);
								loc.getWorld().dropItemNaturally(loc, item); //дропаем с жителя яблоко
								player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.buyok));
							} else { //если предмета не хватает
								player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.nocol.replaceAll("%col%", String.valueOf(main.conf.buycol))));
							}
						} else { //если в руке не тот предмет
							String type = Material.getMaterial(main.conf.buyid).toString();
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.noitem.replaceAll("%id%", type)));
						}
					} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.noperm));
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onEntityDamage(EntityDamageEvent event) { //предотвращение убийства торговцев, если это включено в конфиге
		if (main.conf.enabled && main.conf.nodamage) {
			Entity entity = event.getEntity();
			String name = entity.getCustomName();
			if (entity.getType().toString().equalsIgnoreCase("VILLAGER") && name != null) {
				if (name.equals(main.conf.mobname)) {
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onClickInventory(InventoryClickEvent event) { //обработка кликов в инвентаре для предотвращения создания или кражи предметов от плагина
		if (main.conf.enabled) {
			Player player = (Player) event.getWhoClicked();
			ItemStack click = event.getCurrentItem();
			if (!player.isOp() & !player.hasPermission("newhorsetamed.set") && click != null) {
				if (click.getItemMeta() != null) {
					String type = event.getSlotType().toString();
					if (click.getItemMeta().hasDisplayName() && type.equalsIgnoreCase("RESULT")) { //блокировка создания бирки или яблока
						String name = click.getItemMeta().getDisplayName();
						if (name.equals(main.conf.apple)) {
							event.setCancelled(true);
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.otherrename));
						}
					}
					if (click.getItemMeta().hasDisplayName()) { //блокировка кражи седла / брони с лошади или создания такого же предмета
						String name = click.getItemMeta().getDisplayName();
						boolean sit = false; //блокировка клика, только если игрок сидит на лошади
						int size = horses.size();
						String names = player.getPlayerListName();
						for (int i = 0; i < size; i++) {
							if (horses.get(i).split(";")[0].equals(names)) {
								sit = true;
								break;
							}
						}
						if (sit && name.equals(ChatColor.translateAlternateColorCodes('&', main.conf.saddlename)) | name.equals(ChatColor.translateAlternateColorCodes('&', main.conf.armorname))) {
							event.setCancelled(true);
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.otherrename));
						}
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onInteractEntityHorse(PlayerInteractEntityEvent event) { //дропаем седло с лошади
		if (main.conf.enabled) {
			Entity entity = event.getRightClicked();
			Player player = event.getPlayer();
			if (player.isOp() | player.hasPermission("newhorsetamed.use") && !event.isCancelled()) {
				ItemStack click = player.getInventory().getItemInHand();
				if (entity.getType().toString().equalsIgnoreCase("HORSE") && click != null) {
				if (click.getItemMeta() != null) {
				if (click.getItemMeta().hasDisplayName() && entity.isEmpty() && !((Horse) entity).isTamed()) {
					int clid = click.getTypeId();
					String clname = click.getItemMeta().getDisplayName();
					if (clid == main.conf.appleid && clname.equals(ChatColor.translateAlternateColorCodes('&', main.conf.apple))) { //если кликнули яблоком по лошади и есть разрешение
						event.setCancelled(true);
						Random rand = new Random(); //узнаем вероятность
						rand.setSeed(rand.nextLong());
						int ver = rand.nextInt(100)+1;
						
						Date date = new Date();
						long stamp = date.getTime();
						
						String pname = player.getPlayerListName();
						int ws = times.size();
						int pind = -1; //запоминаем позицию
						boolean w = false;
						long wsec = 0;
						for (int i = 0; i < ws; i++) {
							String dat[] = times.get(i).split(";");
							if (dat[0].equals(pname)) {
								long stamp2 = 0;
								try {
									stamp2 = Long.parseLong(dat[1]);
								} catch(Exception e) {e.printStackTrace();}
								long ch = (stamp / 1000) - (stamp2 / 1000);
								wsec = main.conf.wait - ch;
								if (ch < main.conf.wait) w = true;
								pind = i;
								break;
							}
						}
						
						if (!w) {
							int newamount = click.getAmount()-1;
							if (newamount > 0) {
								click.setAmount(newamount);
								player.getInventory().setItemInHand(click);
							} else { //отнимаем яблоко, если кол-во 0 или отрицательное
								player.getInventory().setItemInHand(null);
							}
						} else {
							player.updateInventory();
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.waitmes.replaceAll("%sec%", String.valueOf(wsec))));
						}
						
						if (ver <= main.conf.percent && !w || player.isOp() | player.hasPermission("newhorsetamed.donator")) { //если выпала вероятность или есть разрешение
							int r = rand.nextInt(main.conf.packs.length);
							pack p = main.conf.packs[r]; //выбираем пак случайным образом
							Location loc = entity.getLocation();
							ItemStack item = new ItemStack(329); //создаем седло
							ItemMeta meta = item.getItemMeta();
							meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', p.displayname));
							ArrayList<String> list = new ArrayList<String>();
							list.add(ChatColor.translateAlternateColorCodes('&', p.lore));
							if (!main.conf.hide) {
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Тип: " + "&c" + p.type));
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Цвет: " + "&c" + p.color));
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Стиль: " + "&c" + p.style));
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Ребенок: " + "&c" + String.valueOf(p.baby).replaceAll("true", "да").replaceAll("false", "нет")));
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Высота прыжка: " + "&c" + String.valueOf(p.jump)));
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Есть сундук: " + "&c" + String.valueOf(p.chest).replaceAll("true", "да").replaceAll("false", "нет")));
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Есть седло: " + "&c" + String.valueOf(p.saddle).replaceAll("true", "да").replaceAll("false", "нет")));
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Броня: " + "&c" + p.armor));
							list.add(ChatColor.translateAlternateColorCodes('&', "&6Можно бить: " + "&c" + String.valueOf(p.damage).replaceAll("true", "да").replaceAll("false", "нет")));
							}
							int s = list.size();
							meta.setLore(list.subList(0, s));
							item.setItemMeta(meta);
							loc.getWorld().dropItemNaturally(loc, item); //дропаем
							entity.remove();
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.tamedtoplay));
							if (pind > -1) {
								times.remove(pind); //удаляем старые данные
							}
							if (main.conf.tamedmess) { //отправляем уведомление
								int psize = Bukkit.getServer().getOnlinePlayers().size();
								Iterator<? extends Player> iter =  Bukkit.getServer().getOnlinePlayers().iterator();
								for (int i = 0; i < psize; i++) {
									Player plr = iter.next();
									if (plr.hasPermission("newhorsetamed.notify") | plr.isOp()) plr.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.tamednotif.replaceAll("%nick%", player.getDisplayName()).replaceAll("%name%", p.displayname)));
								}
								main.log.info(ChatColor.translateAlternateColorCodes('&', main.conf.tamednotif.replaceAll("%nick%", player.getDisplayName())));
							}
						} else {
							if (!w) {
								if (pind > -1) {
									times.remove(pind); //удаляем старые данные
								}
								times.add(pname + ";" + String.valueOf(stamp)); //отправляем данные о задержке
								player.sendMessage(ChatColor.translateAlternateColorCodes('&', main.conf.notamed));
							}
						}
					}
				}}}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void PlayerInteract(PlayerInteractEvent event) { //обрабатываем клики, для посадки на лошадь
		if (main.conf.enabled) {
			Player player = event.getPlayer();
			String action = event.getAction().toString();
			if (player.isOp() | player.hasPermission("newhorsetamed.use") && action.equalsIgnoreCase("RIGHT_CLICK_BLOCK")) { //если есть разрешение
				ItemStack click = player.getInventory().getItemInHand();
				if (click != null) {
				if (click.getItemMeta() != null) {
				if (click.getItemMeta().hasLore() && click.getItemMeta().hasDisplayName() && click.getTypeId() == 329) {
					String l[] = new String[click.getItemMeta().getLore().size()];
					if (!main.conf.hide & l.length == 10 || main.conf.hide & l.length == 1) { //если предмет приблизительно тот
						for (int i = 0; i < l.length; i++) { //собираем инфу
							l[i] = ChatColor.translateAlternateColorCodes('&', click.getItemMeta().getLore().get(i));
						}
						String dname = ChatColor.translateAlternateColorCodes('&', click.getItemMeta().getDisplayName());
						String lore = ChatColor.translateAlternateColorCodes('&', l[0]);
						
						String type = " ";
						String color = " ";
						String style = " ";
						String baby = " ";
						String jump = " ";
						String chest = " ";
						String saddle = " ";
						String armor = " ";
						String damage = " ";
						if (!main.conf.hide) {
						type = ChatColor.translateAlternateColorCodes('&', l[1]);
						color = ChatColor.translateAlternateColorCodes('&', l[2]);
						style = ChatColor.translateAlternateColorCodes('&', l[3]);
						baby = ChatColor.translateAlternateColorCodes('&', l[4]);
						jump = ChatColor.translateAlternateColorCodes('&', l[5]);
						chest = ChatColor.translateAlternateColorCodes('&', l[6]);
						saddle = ChatColor.translateAlternateColorCodes('&', l[7]);
						armor = ChatColor.translateAlternateColorCodes('&', l[8]);
						damage = ChatColor.translateAlternateColorCodes('&', l[9]);
						}
						boolean ok = false;
						int np = 0;
						pack p[] = main.conf.packs;
						for (int i = 0; i < p.length; i++) { //проверяем инфу
							String dn = ChatColor.translateAlternateColorCodes('&', p[i].displayname);
							String ll = ChatColor.translateAlternateColorCodes('&', p[i].lore);
							String t = ChatColor.translateAlternateColorCodes('&', "&6Тип: " + "&c" + p[i].type);
							String c = ChatColor.translateAlternateColorCodes('&', "&6Цвет: " + "&c" + p[i].color);
							String s = ChatColor.translateAlternateColorCodes('&', "&6Стиль: " + "&c" + p[i].style);
							String b = ChatColor.translateAlternateColorCodes('&', "&6Ребенок: " + "&c" + String.valueOf(p[i].baby).replaceAll("true", "да").replaceAll("false", "нет"));
							String j = ChatColor.translateAlternateColorCodes('&', "&6Высота прыжка: " + "&c" + String.valueOf(p[i].jump));
							String cc = ChatColor.translateAlternateColorCodes('&', "&6Есть сундук: " + "&c" + String.valueOf(p[i].chest).replaceAll("true", "да").replaceAll("false", "нет"));
							String sa = ChatColor.translateAlternateColorCodes('&', "&6Есть седло: " + "&c" + String.valueOf(p[i].saddle).replaceAll("true", "да").replaceAll("false", "нет"));
							String ar = ChatColor.translateAlternateColorCodes('&', "&6Броня: " + "&c" + p[i].armor);
							String d = ChatColor.translateAlternateColorCodes('&', "&6Можно бить: " + "&c" + String.valueOf(p[i].damage).replaceAll("true", "да").replaceAll("false", "нет"));
							if (!main.conf.hide) {
							if (
									dname.equals(dn) &&
									lore.equals(ll) &&
									type.equals(t) &&
									color.equals(c) &&
									style.equals(s) &&
									baby.equals(b) &&
									jump.equals(j) &&
									chest.equals(cc) &&
									saddle.equals(sa) &&
									armor.equals(ar) &&
									damage.equals(d)
									) {
								ok = true;
								np = i; //запоминаем номер пака
								break;
							}
							} else {
								if (
										dname.equals(dn) &&
										lore.equals(ll)
										) {
									ok = true;
									np = i; //запоминаем номер пака
									break;
								}
							}
								
						}
						if (ok) { //если совпало с паком
							event.setCancelled(true);
							Location loc = player.getLocation();
							loc.setY(loc.getY()+1);
							Entity entity = loc.getWorld().spawnEntity(loc, EntityType.valueOf("HORSE"));
							type = main.conf.packs[np].type;
							color = main.conf.packs[np].color;
							style = main.conf.packs[np].style;
							boolean babyy = main.conf.packs[np].baby;
							boolean chestt = main.conf.packs[np].chest;
							boolean saddlee = main.conf.packs[np].saddle;
							String armorr = main.conf.packs[np].armor;
							double jumpp = main.conf.packs[np].jump;
							((Horse) entity).setVariant(Variant.valueOf(type));
							((Horse) entity).setColor(Color.valueOf(color));
							((Horse) entity).setStyle(Style.valueOf(style));
							if (babyy) ((Horse) entity).setBaby(); else ((Horse) entity).setAdult();
							((Horse) entity).setJumpStrength(jumpp);
							if (chestt) ((Horse) entity).setCarryingChest(true); else ((Horse) entity).setCarryingChest(false);
							if (saddlee) {
								ItemStack saddleee = new ItemStack(329);
								saddleee.setAmount(1);
								ItemMeta smeta = saddleee.getItemMeta();
								smeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.conf.saddlename));
								saddleee.setItemMeta(smeta);
								((Horse) entity).getInventory().setSaddle(saddleee);
							}
							if (armorr.equalsIgnoreCase("IRON")) {
								ItemStack harmor = new ItemStack(417);
								harmor.setAmount(1);
								ItemMeta hmeta = harmor.getItemMeta();
								hmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.conf.armorname));
								harmor.setItemMeta(hmeta);
								((Horse) entity).getInventory().setArmor(harmor);
							} else if (armorr.equalsIgnoreCase("GOLD")) {
								ItemStack harmor = new ItemStack(418);
								harmor.setAmount(1);
								ItemMeta hmeta = harmor.getItemMeta();
								hmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.conf.armorname));
								harmor.setItemMeta(hmeta);
								((Horse) entity).getInventory().setArmor(harmor);
							} else if (armorr.equalsIgnoreCase("DIAMOND")) {
								ItemStack harmor = new ItemStack(419);
								harmor.setAmount(1);
								ItemMeta hmeta = harmor.getItemMeta();
								hmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', main.conf.armorname));
								harmor.setItemMeta(hmeta);
								((Horse) entity).getInventory().setArmor(harmor);
							}
							horses.add(player.getPlayerListName() + ";" + entity.getUniqueId().toString() + ";" + String.valueOf(np)); //запоминаем
							((Horse) entity).setTamed(true);
							((Horse) entity).setOwner((AnimalTamer) player);
							entity.setPassenger((Entity) player);
							ItemStack item = new ItemStack(0); //удаляем седло, потом выдаст по номеру пака
							player.getInventory().setItemInHand(item);
						}
					}
				}}}
			}
		}
	}
	
	boolean SaddleGive(Player player, Entity entity) { //выдаем седло (вызывается ивентами)
		boolean result = false;
		if (player.hasPermission("newhorsetamed.use") | player.isOp()) {
			if (entity.getType().toString().equalsIgnoreCase("HORSE")) {
				int size = horses.size();
				String name = player.getPlayerListName();
				String uuid = entity.getUniqueId().toString();
				boolean ok = false;
				int np = 0;
				for (int i = 0; i < size; i++) { //проверка, есть ли запись в БД
					String str = horses.get(i);
					String n = str.split(";")[0];
					String u = str.split(";")[1];
					if (name.equals(n) && uuid.equals(u)) {
						ok = true;
						try {
							np = Integer.parseInt(str.split(";")[2]);
						} catch(Exception e) {e.printStackTrace();}
						horses.remove(i);
						break;
					}
				}
				if (ok) { //если лошадка наша
					pack p = main.conf.packs[np];
					main.GiveSaddle(player.getPlayerListName(), p); //выдаем седло
					if (p.chest) { //выдаем инвентарь с сундука, если он есть
						ItemStack inv[] = player.getInventory().getContents();
						ItemStack hoinv[] = ((Horse) entity).getInventory().getContents();
						if (hoinv != null) {
							if (hoinv.length > 0) {
								for (int i = 0; i < hoinv.length; i++) {
									if (hoinv[i] != null) {
										ItemStack no = hoinv[i];
										for (int n = 0; n < inv.length; n++) {
											if (inv[n] == null) {
												if (hoinv[i].getItemMeta().hasDisplayName()) {
													name = hoinv[i].getItemMeta().getDisplayName();
													if (name.equals(ChatColor.translateAlternateColorCodes('&', main.conf.saddlename)) | name.equals(ChatColor.translateAlternateColorCodes('&', main.conf.armorname))) {
														no = null;
														break; //седло и броньку не выдаем
													}
												}
												inv[n] = hoinv[i];
												no = null;
												break;
											}
										}
										if (no != null) { //если нет места - дропаем
											Location loc = player.getLocation();
											loc.getWorld().dropItemNaturally(loc, no);
										}
									}
								}
								player.getInventory().setContents(inv);
							}
						}
					}
					entity.remove();
					result = true;
				}
			}
		}
		return result;
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onVehicleExit(VehicleExitEvent event) { //обработка вставания с лошади
		if (main.conf.enabled) {
			if (event.getExited().getType().toString().equalsIgnoreCase("PLAYER")) {
				Player player = (Player) event.getExited();
				Entity entity = event.getVehicle();
				SaddleGive(player, entity);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerQuit(PlayerQuitEvent event) { //обработка выхода с сервера
		Player player = event.getPlayer();
		if (player.isInsideVehicle()) {
			Entity entity = player.getVehicle();
			if (SaddleGive(player, entity)) {
				Location loc = player.getLocation();
				int y = loc.getBlockY();
				loc.setY(y+1);
				player.teleport(loc);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onDamageToHorse(EntityDamageEvent event) { //обработка урона
		if (main.conf.enabled) {
			if (event.getEntityType().toString().equalsIgnoreCase("HORSE")) {
				String uuid = event.getEntity().getUniqueId().toString();
				int s = horses.size();
				for (int i = 0; i < s; i++) {
					if (uuid.equals(horses.get(i).split(";")[1])) { //если бьют нашу лошадку
						event.setCancelled(true);
						break;
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onInteractEntitySetVillager(PlayerInteractEntityEvent event) { //еще одна обработка клика по жителю
		if (main.conf.enabled) {
			Player player = event.getPlayer();
			Entity entity = event.getRightClicked();
			int item = 0;
			String birk = " ";
			if (player.getInventory().getItemInHand() != null) {
				item = player.getInventory().getItemInHand().getTypeId();
				if (player.getInventory().getItemInHand().getItemMeta() != null) {
					if (player.getInventory().getItemInHand().getItemMeta().hasDisplayName()) {
						birk = player.getInventory().getItemInHand().getItemMeta().getDisplayName();
					}
				}
			}
			if (player.isOp() | player.hasPermission("newhorsetamed.set") && entity.getType().toString().equalsIgnoreCase("VILLAGER") & item == 421 & birk.equals(ChatColor.translateAlternateColorCodes('&', main.conf.mobname))) {
				event.setCancelled(true); //чтобы можно было переименовать
				entity.setCustomName(birk);
			}
		}
	}
	
}